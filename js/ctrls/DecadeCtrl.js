"use strict"
test.controller('DecadeCtrl', ['$scope', 'decadeService',
    function ($scope, decadeService) {
        decadeService.get().then(res => {
            $scope.labels = res.labels;
            $scope.data = res.data;
        });
    }]);