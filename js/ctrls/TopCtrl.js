"use strict"
test.controller('TopCtrl', ['$scope', 'apiService', 'favouriteService',
    function ($scope, apiService, favouriteService) {
        $scope.films = [];

        apiService.get('top').then(res=>$scope.films = res);

        $scope.addFavFilms = film => { 
            film.favourite = true;            
            favouriteService.set(film);
        }

        $scope.removeFavFilms = film => {
            favouriteService.remove(film.idIMDB);
            film.favourite = false;            
        }

        $scope.getTrailer = film => {
            apiService.getTrailer('trailer', film.title).then(res=>{
                film.trailers = res;
            });
        }
    }]);