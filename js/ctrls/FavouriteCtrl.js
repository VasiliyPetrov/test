"use strict"
test.controller('FavouriteCtrl', ['$scope', 'favouriteService', 'apiService',
    function ($scope, favouriteService, apiService) {
        $scope.films = favouriteService.get();
        $scope.removeFavFilms = (film) => {
            favouriteService.remove(film.idIMDB);
            $scope.films = favouriteService.get();
        }
        $scope.getTrailer = film => {
            apiService.getTrailer('trailer', film.title).then(res => {
                film.trailers = res;
            });
        }
    }]);