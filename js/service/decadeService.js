"use strict"
test.service('decadeService', ['apiService',
    function (apiService) {
        let sortDecade = {};
        this.get = () => { 
            if (!sortDecade.labels) {
                sortDecade.labels = [];
                sortDecade.data = [];
                return apiService.get('top').then(res => {
                    res.sort((a, b) => {
                        if (a.year < b.year) {
                            return -1;
                        } else if (a.year > b.year) {
                            return 1;
                        }
                        return 0;
                    });
                    res.forEach((film) => {
                        var key = film.year[2] + '0th';
                        var index = sortDecade.labels.indexOf(key);
                        if (index < 0) {
                            sortDecade.labels.push(key);
                            sortDecade.data.push(1);
                        } else {
                            sortDecade.data[index]++;
                        }
                    }, this);
                    return sortDecade;
                })
            }
            return new Promise((r) => r(sortDecade));
        }
    }]); 