"use strict"
test.service('localStorService', ['$window',
    function ($window) {
        const LS = $window.localStorage;
        let init = () => {
            LS.token = 'token=42ab04d5-7fb7-4138-bdb9-d87842a4db7a';
            LS.url = 'http://www.myapifilms.com/';
            LS.favourite = '[]';
        }
        !LS.length && init();
        this.get = () => {
            return LS;
        }
    }]); 