"use strict"
test.service('apiService', ['$http', 'localStorService', '$timeout',
    function ($http, localStorService, $timeout) {
        const LS = localStorService.get();
        const urls = {
            top: {
                api: 'imdb/top?start=1&end=20&',
                format: '&format=json&data=1'
            },
            trailer: {
                api: 'trailerAddict/taapi?',
                format: '&count=8&credit=&format=json&film='
            }
        };
        let films = [], lock = false, promise;

        let checkFavourite = (movies) => {
            let favFilms = JSON.parse(LS.favourite);
            movies.forEach(function (film) {
                film.favourite = favFilms.some(favfilm => favfilm.idIMDB == film.idIMDB);
            }, this);
            return films = movies;
        }

        this.getTrailer = (url, filmTitle) => {
            return $http.get(LS.url + urls[url].api + LS.token + urls[url].format + filmTitle)
                .then(res => {
                    return res.data.data ? res.data.data.trailer : []
                })
                .catch(er => console.log(er));
        }

        this.get = url => {
            if (lock) return promise;
            if (!films.length) {
                lock = true;
                return promise = $http.get(LS.url + urls[url].api + LS.token + urls[url].format)
                    .then(res => {
                        lock = false;
                        return checkFavourite(res.data.data ? res.data.data.movies : []);
                    })
            }
            return new Promise((r) => r(checkFavourite(films)));
        }
    }]); 