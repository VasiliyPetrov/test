"use strict"
test.service('favouriteService', ['localStorService',
    function (localStorService) {
        const LS = localStorService.get();
        let favFilms =  JSON.parse(LS.favourite);
        let setToLS = () => {
            LS.favourite = JSON.stringify(favFilms);
        } 
        this.set = (film) =>{
            favFilms.push(film);
            setToLS();
        }
        this.remove = (idIMDB) =>{
            favFilms.forEach(function(el, indx) {
                if(idIMDB === el.idIMDB) return favFilms.splice(indx, 1);
            }, this);
            setToLS();            
        }
        this.get = () => JSON.parse(LS.favourite);
    }]); 