"use strict"
const test = angular.module('test', ['ui.router', 'chart.js']);

test.config(['$stateProvider', '$locationProvider', '$urlRouterProvider',
    function ($stateProvider, $locationProvider, $urlRouterProvider) {

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('top', {
                url: '/',
                templateUrl: '../templates/top.html',
                controller: 'TopCtrl'
            })
            .state('decade', {
                url: '/decade',
                templateUrl: '../templates/decade.html',
                controller: 'DecadeCtrl'
            })
            .state('favourite', {
                url: '/favourite',
                templateUrl: '../templates/top.html',
                controller: 'FavouriteCtrl'
            })
    }]);
