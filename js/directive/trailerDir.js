test.directive('trailerDir', function () {
    return {
        restrict: 'E',
        link: function (scope, el) {
            scope.openTrailer = trailer => {
                scope.showTrailer = true;
                el[0].innerHTML = trailer.embed;
            }
            scope.closeTrailer = () => {
                el[0].innerHTML = '';
                scope.showTrailer = false;
            }
        }
    };
});